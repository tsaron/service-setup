from marshmallow.fields import Int, Str, Email, Date, Method, List, Nested
from pylib.validation.schemas import MongoSchema
from pylib.validation.validators import enum

tag_check = enum('Tech', 'Psychology', 'Economics')


def phone_check(x):
    return len(x) == 11 or len(x) == 0


class Author(MongoSchema):
    firstname = Str(required=True)
    surname = Str(required=True)
    email = Email()
    homephone = Str(validate=phone_check)
    officephone = Str(validate=phone_check)
    address = Str()
    passport = Method('get_image')

    def get_image(self, obj):
        return obj['passport'].decode('utf-8')


class BlogEntry(MongoSchema):
    title = Str(required=True)
    content = Str(required=True)
    author = Nested(Author, dump_only=True)
