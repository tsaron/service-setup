from falcon import HTTPConflict, HTTPNotFound

# Error for http conflict, inherits
class BlogEntryExists(HTTPConflict):

    def __init__(self):
        super().__init__(
            'Duplicate error',
            "This blog has already been created")


class UnknownBlogEntry(HTTPNotFound):

    def __init__(self):
        super().__init__(
            title="Not found error",
            description="This blog entry does not exist")
